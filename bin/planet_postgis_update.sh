#!/bin/sh
#
# Script updates planet.osgeo.org feed:
#	1. svn update planet configuration
#	2. ensure non-root files ownership
#	3. run planet.py
#
# Mateusz Loskot <mateusz@loskot.net>
#
# Configuration
VENUSDIR=/var/www/venus
VENUSCFG=${VENUSDIR}/planetpostgis.ini
VENUSLOG=${VENUSDIR}/planet.log
PLANETDIR=/var/www/postgis_planet
# Script
date > ${VENUSLOG}
cd ${VENUSDIR} || \
	{ echo "Directory '${VENUSDIR}' does not exist" >> ${VENUSLOG}; exit 1; }
svn up || \
	{ echo "svn update failed in '${VENUSDIR}'" >> ${VENUSLOG}; exit 1; }
/usr/bin/python ${VENUSDIR}/planet.py --verbose ${VENUSCFG} >> ${VENUSLOG} 2>&1 || \
	{ echo "planet.py failed in '${VENUSDIR}'" >> ${VENUSLOG}; exit 1; }
cp ${VENUSLOG} ${PLANETDIR}
chown -R www-data:www-data ${PLANETDIR} || \
	{ echo "chown failed in '${PLANETDIR}'" >> ${VENUSLOG}; exit 1; }

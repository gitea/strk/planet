<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

### Fancy Planet HTML template.
### 
### When combined with the stylesheet and images in the output/ directory
### of the Planet source, this gives you a much prettier result than the
### default examples template and demonstrates how to use the config file
### to support things like faces
### 
### For documentation on the more boring template elements, see
### examples/config.ini and examples/index.html.tmpl in the Planet source.

<head>
<title><TMPL_VAR name></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="generator" content="<TMPL_VAR generator ESCAPE="HTML">">
<link rel="stylesheet" href="planet.css" type="text/css">
<TMPL_IF feedtype>
<link rel="alternate" href="<TMPL_VAR feed ESCAPE="HTML">" title="<TMPL_VAR channel_title_plain ESCAPE="HTML">" type="application/<TMPL_VAR feedtype>+xml">
</TMPL_IF>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/mootools/1.2.3/mootools-yui-compressed.js"></script>
<script type="text/javascript" src="planet.js"></script>
</head>

<body>
<h1>Welcome to <TMPL_VAR name></h1>
<TMPL_VAR admin>


<div class="items">
<TMPL_LOOP Items>
<TMPL_IF new_date>
<h2><TMPL_VAR new_date></h2>
</TMPL_IF>

### Planet provides template variables for *all* configuration options for
### the channel (and defaults), even if it doesn't know about them.  We
### exploit this here to add hackergotchi faces to our channels.  Planet
### doesn't know about the "face", "facewidth" and "faceheight" configuration
### variables, but makes them available to us anyway.

<h3><a href="<TMPL_VAR channel_link ESCAPE="HTML">" title="<TMPL_VAR channel_title_plain ESCAPE="HTML">"><TMPL_VAR channel_name></a></h3>
<TMPL_IF channel_face>
<img class="face" src="heads/<TMPL_VAR channel_face ESCAPE="HTML">" alt="">
</TMPL_IF>


<div class="entrygroup" id="<TMPL_VAR id>"<TMPL_IF channel_language> lang="<TMPL_VAR channel_language>"</TMPL_IF>>
<TMPL_IF title>
<h4<TMPL_IF title_language> lang="<TMPL_VAR title_language>"</TMPL_IF>><a href="<TMPL_VAR link ESCAPE="HTML">"><TMPL_VAR title></a></h4>
</TMPL_IF>
<div class="entry">
<div class="content"<TMPL_IF content_language> lang="<TMPL_VAR content_language>"</TMPL_IF>>
<TMPL_VAR content>
</div>

### Planet also makes available all of the information from the feed
### that it can.  Use the 'planet-cache' tool on the cache file for
### a particular feed to find out what additional keys it supports.
### Comment extra fields are 'author' and 'category' which we
### demonstrate below.

<p class="date">
<a href="<TMPL_VAR link ESCAPE="HTML">"><TMPL_IF author>by <TMPL_VAR author ESCAPE="HTML"> at </TMPL_IF><TMPL_VAR date><TMPL_IF category> under <TMPL_VAR category></TMPL_IF></a>
</p>
</div>
</div>

</TMPL_LOOP>
</div> <!-- end items-->

<div class="sidebar">

    <a href="http://postgis.net" target="_blank"><img src="images/logo.png" alt="PostGIS" title="For more information about PostGIS"/></a>
    <a href="http://www.osgeo.org" target="_blank"><img src="images/OSGeo_project.png" alt="An OSGeo Project" /></a>
    <p>File a bug or enhancement request <a href="http://trac.osgeo.org/postgis" target="_blank">PostGIS wiki and bug tracker</a> </p>
    <p>
		<a href="http://planet.postgis.net">Planet PostGIS</a> is a window into PostGIS, a spatial extension, for PostgreSQL .  Here you'll discover neat tricks for getting the most out of PostGIS
		geometry, geography, topology, and raster. Find out how others are leveraging the power of PostGIS.
		If you'd like your PostGIS content aggregated here, please drop me a line at lr at pcorp.us
		with your blog name, preferred link url, and your PostGIS feed url. </p>
	<p>You may also want to be aggregated on our parent organization OSGeo
		please see the 
		<a href="http://wiki.osgeo.org/wiki/PlanetOSGeo">Planet OSGeo</a>
		wiki page. 
	</p>
	
	<p><strong>Tip:</strong> use j/k to navigate</p>
    
    <hr>   
    
    <h2>Events</h2>

 <div style="text-align:center;">
<a href="http://www.pgconf.us/2017"><img src="http://www.pgconf.us/static/images/pgconfus.png" alt="PGConf US April 18-20, 2017 in NYC" width="150"><br><b>PGConf US March 28th-31st, 2017 in Jersey City,NJ</b></a>

<!-- <a href="http://2016.foss4g-na.org"><img src="https://2016.foss4g-na.org/sites/all/themes/eclipsecon_base/themes/foss4g2016/logo.png" alt="FOSS4G North America 2016, May 2-5" height="49" width="200"></a> -->

<a href="http://2017.foss4g.org"><img src="http://2017.foss4g.org/images/header.png" alt="FOSS4G 2017, August 14-18th Boston, MA, USA" width="150"></a>
</div>

	<p>If you have any upcoming PostGIS related events you'd like posted on this page and PostGIS site, please write to psc at postgis.net</p>
    <hr />    
    
    <h2>Subscriptions</h2>
    
    <ul>
		<TMPL_LOOP Channels>
		<li>
			<a href="<TMPL_VAR url ESCAPE="HTML">" title="subscribe">
				<img src="images/feed-icon-10x10.png" alt="(feed)">
			</a> 
			<a 
				<TMPL_IF link>href="<TMPL_VAR link ESCAPE="HTML">" </TMPL_IF>
				<TMPL_IF message>class="message" title="<TMPL_VAR message ESCAPE="HTML">"</TMPL_IF>
				<TMPL_UNLESS message>title="<TMPL_VAR title_plain ESCAPE="HTML">"</TMPL_UNLESS>><TMPL_VAR name>
			</a>
		</li>
		</TMPL_LOOP>
    </ul>
    
    
    <hr>
    
    <h2>Communities</h2>
    <ul>
		<li>Chat on the <a href="irc://irc.freenode.net:6667/postgis" title="PostGIS IRC Channel">#postgis</a> IRC channel</li>
		<li>Find <a href="http://www.osgeo.org/content/chapters/index.html">OSGeo local chapters</a> around the world</li>
		<li>Visit our sister <a href="http://planetgs.com/" title="Planet Geospatial">Planet Geospatial</a></li>
    </ul>
    
    <hr>    
    
    <h2>Subscribe</h2>
	<ul>
		<li><a href="atom.xml">Atom 1.0</a></li>
		<li><a href="rss20.xml">RSS 2.0</a></li>
		<li><a href="rss10.xml">RSS 1.0</a></li>
		<li><a href="foafroll.xml">FOAF</a></li>
		<li><a href="opml.xml">OPML</a></li>
	</ul>
	
	<hr>
	
	<h2>Colophon</h2>
	<p>
		Brought to you by the <a href="http://www.intertwingly.net/code/venus/">Planet Venus</a> 
		aggregator and <a href="http://www.python.org/">Python</a> 
	    Design based on 
		<a href="http://planet.osgeo.org/">Planet OSGeo</a>. 
	</p>
	
	<hr>

    <p>
    <strong>Last updated:</strong><br>
    <TMPL_VAR date><br>
    <em>All times are UTC.</em><br>
    <br>
    Powered by:<br>
    <a href="http://www.planetplanet.org/"><img src="images/planet.png" width="80" height="15" alt="Planet" border="0"></a>
    </p>

</div>

</body>

</html>
